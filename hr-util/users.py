import pwd

def fetch_users():
    pwds = pwd.getpwall()
    users = []
    for pw in pwds:
        home = pw.pw_dir
        id = pw.pw_uid
        if id >= 1000 and 'home' in home:
            users.append({
                'name': pw.pw_name,
                'id': id,
                'home': home,
                'shell': pw.pw_shell,
            })
    return users
