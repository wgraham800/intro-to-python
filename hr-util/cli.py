from argparse import ArgumentParser

def create_parser():
    parser = ArgumentParser(description="""
    Export users from the /etc/passwd file on a linux operating
    system to stdout or a file, in either CSV or JSON formats.
    """)

    parser.add_argument('--format', '-f',
        help = 'Format specification of results (csv or json)',
        default = 'json',
        choices = ['json', 'csv'],
        type = str.lower)
    parser.add_argument('--dir', '-d', help = 'Path to export results to')

    return parser
    
import sys
import users as u
import export

args = create_parser().parse_args()
users = u.fetch_users()

if args.dir:
    outfile = open(args.dir, 'w', newline='')
else:
    outfile = sys.stdout

if args.format == 'csv':
    export.export_as_csv(outfile, users)
else:
    export.export_as_json(outfile, users)
