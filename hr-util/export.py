import json
import csv

def export_as_json(outfile, data):
    json.dump(data, outfile, indent=4)
    outfile.close()

def export_as_csv(outfile, users):
    outfile.write('name,id,home,shell\n')

    writer = csv.writer(outfile)
    rows = [[user['name'], user['id'], user['home'], user['shell']] for user in users]
    writer.writerows(rows)

    outfile.close()
