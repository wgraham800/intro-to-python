from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Ticket(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.Integer, nullable=False)
    url = db.Column(db.String(255))
    name = db.Column(db.String(128), nullable=True)

    statuses = {
        0: 'Reported',
        1: 'In Progress',
        2: 'In Review',
        3: 'Resolved',
    }

    @property
    def status_string(self):
        return self.statuses[self.status]

    def to_json(self):
        return {
            'id' : self.id,
            'status' : self.status_string,
            'url' : self.url,
            'name' : self.name
        }
